FROM php:8.2-cli
COPY . /usr/src/homepage
WORKDIR /usr/src/homepage
CMD [ "php", "-S", "0.0.0.0:80", "-t", "./public" ]