<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\ResourceController;
use App\Http\Controllers\TimetableController;

use App\Http\Middleware\DiscordLogin;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Routes available to everyone */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('resources', [ResourceController::class, 'resources'])->name('resources');
Route::get('timetable', [TimetableController::class, 'timetable'])->name('timetable');

/* Routes available to admins */
Route::get('/admin', [AdminController::class, 'index'])->name('admin')->middleware('isAdmin');
Route::get('/update/website', [AdminController::class, 'updateWebsite'])->name('updateWebsite')->middleware('isAdmin');

/* Routes just for handling logins */
Route::get('/login', [DiscordLogin::class, 'login'])->name('login');
Route::get('/logout', [DiscordLogin::class, 'logout'])->name('logout');
Route::get('/auth', [DiscordLogin::class, 'auth']);

Route::post('/schedule/add', [AdminController::class, 'scheduleAdd'])->name('scheduleAdd')->middleware('isAdmin');
Route::get('/schedule/remove/{id}', [AdminController::class, 'scheduleRemove'])->name('scheduleRemove')->middleware('isAdmin');

// Force HTTPS encoding on all files, in case the site is hosted behind a reverse-proxy
URL::forceScheme('https');
