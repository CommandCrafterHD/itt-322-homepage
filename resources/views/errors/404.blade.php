@extends('layouts.error')

@section('body')

    <i class="fa-solid fa-magnifying-glass mb-5 error-icon looking-glass-anim"></i>
    <h1>404</h1>
    <p>Die Seite scheint nicht zu existieren, sorry</p>
    <a href="{{ route('index') }}" class="btn btn-success">Zurück zur Startseite</a>

@endsection
