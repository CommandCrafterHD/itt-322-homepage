@extends('layouts.error')

@section('body')

    <i class="fa-solid fa-gears mb-5 error-icon"></i>
    <h1>500</h1>
    <p>Interner Server fehler, versuchs gleich nochmal!</p>
    <a href="{{ route('index') }}" class="btn btn-success">Zurück zur Startseite</a>

@endsection
