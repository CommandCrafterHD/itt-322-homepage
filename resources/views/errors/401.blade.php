@extends('layouts.error')

@section('body')

    <i class="fa-solid fa-triangle-exclamation mb-5 error-icon"></i>
    <h1>401</h1>
    <p>Dir fehlen wohl die Rechte hierfür</p>
    <a href="{{ route('index') }}" class="btn btn-success">Zurück zur Startseite</a>

@endsection
