<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="w-100 h-100">
<head>
    <meta charset="utf-8">
    <title>ITT-322 | {{ $exception->getStatusCode() }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="bg-dark w-100 h-100 text-white d-flex justify-content-center align-items-center flex-column">
    @yield('body')
</body>
