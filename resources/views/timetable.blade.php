@extends('layouts.app')

@section('body')
<div class="container m-2">
    <h1 class="text-center">Stundenplan</h1>
    <p>Momentan muesst ihr noch manuell die Klasse angeben aber wir arbeiten bereits an einer Portierung des Scrapers vom <a href="https://gitlab.com/CommandCrafterHD/ITT-Discord-Bot">ITT-Discord-Bot</a> fuer die Website :3 <br>~Tila</p>
    <iframe class="embed-responsive embed-responsive-16by9" src="https://mese.webuntis.com/WebUntis/?school=BK-Technik-Siegen#/basic/timetable" style="border:1px #ffffff dashed;" name="myiFrame" scrolling="yes" frameborder="0" marginheight="0px" marginwidth="0px" height="800px" width="1200px" allowfullscreen></iframe>
</div>
@endsection