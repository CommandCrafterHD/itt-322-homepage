@extends('layouts.app')

@section('body')
<h1 class="m-3">Tutorials und Bücher</h1>
<div class="container m-2">
    <h2 class="text-left">Rust</h2>
    <p class="text-left">Rust ist eine funktionsorientierte Programmiersprache mit sicherem Memory-Management.</p>
    <a href="https://doc.rust-lang.org/book/" target="_blank">
        <p class="text-left">Rust Beginner's Tutorial</p> 
    </a>
    <a href="{{ asset('storage/programmingrust.pdf') }}" target="_blank">
        <p class="text-left">Programming Rust: Fast, Safe Systems Development</p> 
    </a>
</div>
<div class="container m-2">
    <h2 class="text-left">C</h2>
    <p class="text-left">C ist eine Sprache</p>
    <a href="{{ asset('storage/The_C_Programming_Language_-_2nd_edition.pdf') }}" target="_blank">
        <p class="text-left">The C Programming Langueage 2nd Edition</p> 
    </a>
</div>
<div class="container m-2">
    <h2 class="text-left">Assembly x86</h2>
    <p class="text-left">Die Sprache mit der die x86 Architektur programmiert wird.</p>
    <a href="{{ asset('storage/Assembly.Language.For_.x86.Processors.Kip_.R..Irvine..6ed.Prentice.Hall_.2011www.xuexi111.com_.pdf') }}" target="_blank">
        <p class="text-left">Assembly Language for x86 Processors</p> 
    </a>
</div>
<div class="container m-2">
    <h2 class="text-left">Turbo Pascal</h2>
    <p class="text-left">Seit dem letzten Sportfest hiess Pascal nur noch Turbo Pascal</p>
    <a href="{{ asset('storage/Turbo_Tutor_Version_1.0_Jan85.pdf') }}" target="_blank">
        <p class="text-left">Turbo Tutor</p> 
    </a>
</div>      
@endsection
