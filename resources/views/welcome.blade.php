@extends('layouts.app')

@section('body')
    <div class="p-3 justify-content-center align-items-center d-flex flex-column">
        <h1>Willkommen auf der Website der ITT322 des BKT Siegens</h1>
        <h2>Hier findet ihr alle wichtigen Termine und Infos rund um euren Schultag :3</h2>
        <h4>Diese Website wird maintained durch:</h4>
        <h5><a class="p-2" href="https://xqtc.online">xqtc</a><a class="p-2" href="https://cegledi.net">Bates</a></h5>
        <ul style="width: 25%" class="list-group">
            <li class="list-group-item text-center active text-light border-light">Kalender</li>
            @foreach($scheduleEntries as $entry)
            <li class="list-group-item bg-dark text-light border-light d-flex align-items-center justify-content-center">{{ $entry->title }} <span class="badge bg-danger ms-2">{{ date_format(DateTime::createFromFormat('Y-m-d H:i:s', $entry->date), 'd/m/Y') }}</span></li>
            @endforeach
        </ul>
        <a href="https://www.ikea.com/de/de/p/blahaj-stoffspielzeug-hai-30373588/" target="_blank"><img class="p-2" src="https://c.tenor.com/h6T7YUwIn5UAAAAC/blahaj-go-spinny-blahaj.gif"/></a>

    </div>
@endsection
