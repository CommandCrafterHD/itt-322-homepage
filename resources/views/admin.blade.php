@extends('layouts.app')

@section('body')

<style media="screen">
    .shellOutput {
        background-color: #000;
        color: #fff;
        border: 1px solid #fff;
        border-radius: 5px;
        padding: 5px;
        min-height: 20px;
    }
</style>

<div class="p-5 w-100 h-100 d-flex flex-row">
    <div class="card text-bg-secondary m-2 w-25">
        <div class="card-body">
            <h5 class="card-title">Website</h5>
            <h6 class="card-subtitle mb-2">HEAD: {{ shell_exec('git rev-parse --short HEAD') }}</h6>
            <div id="websiteShell" class="shellOutput">
                <div id="websiteSpinner" class="spinner-border text-success" hidden role="status">
                  <span class="visually-hidden">Loading...</span>
                </div>
            </div>
            <a class="btn btn-success mt-2" onclick="updateWebsite()">Website pullen</a>
        </div>
    </div>
    <div class="card text-bg-secondary m-2">
        <div class="card-body">
            <h5 class="card-title">Termine</h5>
            <ul class="list-group list-group-flush">
            @foreach($scheduleEntries as $entry)
              <li class="list-group-item d-flex bg-secondary text-light"><p class="my-auto flex-fill">{{ $entry->title }} <span class="badge bg-primary">{{ date_format(DateTime::createFromFormat('Y-m-d H:i:s', $entry->date), 'd/m/Y') }}</span> </p><a href="{{ route('scheduleRemove', $entry->id) }}" class="btn btn-danger"><i class="fa-solid fa-trash"></i></a></li>
            @endforeach
            </ul>
            <form class="input-group mt-2" action="{{ route('scheduleAdd') }}" method="post">
                <input type="text" class="input-group-text" placeholder="Title" name="scheduleTitle" aria-label="Schedule Title" required>
                @csrf
                <input type="date" class="input-group-text" name="scheduleDate" required>
                <button type="submit" class="btn btn-success" name="scheduleSubmit"><i class="fa-solid fa-floppy-disk"></i></button>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function updateWebsite() {
        var websiteSpinner = document.getElementById('websiteSpinner')
        websiteSpinner.hidden = false;
        var url = "{{ route('updateWebsite') }}";
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
                var textNode = document.createTextNode(xmlHttp.responseText);  // TODO: Fix linebreaks. Also move all of this into a simpler function
                document.getElementById('websiteShell').appendChild(textNode);
                websiteSpinner.hidden = true;
            }
        }
        xmlHttp.open('GET', url, true);
        xmlHttp.send(null);
    }
</script>

@endsection
