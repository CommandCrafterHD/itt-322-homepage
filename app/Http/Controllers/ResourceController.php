<?php

namespace App\Http\Controllers;

use App\Http\Middleware\DiscordLogin;

class ResourceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function resources()
    {
        return view('resources')->with(['user' => DiscordLogin::getUser()]);
    }
}
