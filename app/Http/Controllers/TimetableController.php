<?php

namespace App\Http\Controllers;

use App\Http\Middleware\DiscordLogin;

class TimetableController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function timetable()
    {
        return view('timetable')->with(['user' => DiscordLogin::getUser()]);
    }
}
