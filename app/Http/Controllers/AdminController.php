<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Middleware\DiscordLogin;
use App\Models\Schedule;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin')->with(['user' => DiscordLogin::getUser(), 'scheduleEntries' => Schedule::all()->sortBy('date')]);
    }

    public function updateWebsite() {
        if (config('app.env') == 'local') {
            exec('git status', $outputArray);
            return implode('</br>', $outputArray);
            // return "Local env, repo not pulled";
        } else {
            exec('git pull', $outputArray);
            return implode('\n', $outputArray);
        }
    }

    public function scheduleAdd(Request $request) {
        $schedule = new Schedule;

        $schedule->title = $request->input('scheduleTitle');
        $schedule->date = $request->input('scheduleDate');
        $schedule->userID = DiscordLogin::getUser()->id;

        $schedule->save();

        return back();
    }

    public function scheduleRemove($id) {
        $schedule = Schedule::where('id', $id)->first();
        if (isset($schedule)) {
            $schedule->delete();
        }
        return back();
    }
}
