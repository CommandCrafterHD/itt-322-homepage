<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Middleware\DiscordLogin;

class IsAdmin {
    public function handle($request, Closure $next) {
        $user = app(DiscordLogin::class)->getUser();
        if ($user == false || !in_array($user->id, ['137259132305539072', '259688615246954497', '379984311212048385', '147356795407433728'])) {
            return abort(401);
        }
        else {
            return $next($request);
        }
    }
}

?>
