<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Middleware\DiscordLogin;

class IsMember {
    public function handle($request, Closure $next) {
        $user = app(DiscordLogin::class)->getUser();
        if ($user == false || !in_array($user->id, ['1013703444693000253'/*Aesf*/, '579235490318778368' /*Ardit*/, '208994559340969984' /*Des*/, '367577283319431168' /*Joel*/, '245111966698438656' /*kavum*/, '303960809539633168' /*Kelly*/, '172978817462173696' /*Nick*/, '328125113079955467' /*Maxim*/, '852612786533564438' /*Maximilian*/, '808069005238992898' /*Mümtaz*/, '166536646588760065' /*Nico*/, '250338517945679872' /*Robin*/ ])) {
            return abort(401);
        }
        else {
            return $next($request);
        }
    }
}

?>
