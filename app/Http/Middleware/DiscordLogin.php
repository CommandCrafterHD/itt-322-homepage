<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Illuminate\Http\URL;

class DiscordLogin extends Middleware
{
    public static function getUser() {
        $token = Session('token');
        if ($token != null) {
            $url = 'https://discord.com/api/users/@me';

            $options = array(
                'http' => array(
                    'header'  => [
                                "Content-type: application/x-www-form-urlencoded",
                                'Authorization: Bearer ' . $token
                                ],
                    'method'  => 'GET'
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            if ($result === FALSE) {
                return redirect('/');  # TODO: Maybe show an error here.
            }

            $result = json_decode($result);
            return $result;
        } else {
            return false;
        }
    }

    public function getToken($code) {
        $url = 'https://discord.com/api/oauth2/token';
        $data = array(
            'client_id' => env('DISCORD_APP_ID', 0),
            'client_secret' => env('DISCORD_APP_SECRET', 0),
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => url('/auth')
        );

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded",
                'method'  => 'POST',
                'content' => http_build_query($data)
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if ($result === FALSE) {
            return redirect('/');  # TODO: Maybe show an error here.
        }

        $result = json_decode($result);
        Session(['token' => $result->access_token]);
    }

    public function auth(Request $request)
    {
        $code = $request->input('code');
        if ($code != null) {
            $this->getToken($code);
            return redirect('/');
        } else {
            return redirect('/'); # TODO: Maybe show an error here.
        }
    }

    public function login() {
        $user = $this->getUser();
        if ($user != false) {
            return redirect('/');
        }
        $redirectUrl = "https://discord.com/api/oauth2/authorize?client_id=1012635549527449630&redirect_uri=" . url('/auth') . "&response_type=code&scope=identify%20guilds";
        return redirect($redirectUrl);
    }

    public function logout() {
        Session()->forget('token');
        return redirect('/');
    }

}
